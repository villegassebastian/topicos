package co.edu.eam.ds.sistemavotacion.model.msj;

import co.edu.eam.ds.sistemavotacion.model.entities.Partido;

public class CandidatoMessage {

    private String codigo;
    private String nombre;
    private Partido partidos;
    private String estado;
    private int numerovotos;


    public CandidatoMessage() {
    }

    public CandidatoMessage(String codigo, String nombre, Partido partidos, String estado, int numerovotos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.partidos = partidos;
        this.estado = estado;
        this.numerovotos = numerovotos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Partido getPartidos() {
        return partidos;
    }

    public void setPartidos(Partido partidos) {
        this.partidos = partidos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getNumerovotos() {
        return numerovotos;
    }

    public void setNumerovotos(int numerovotos) {
        this.numerovotos = numerovotos;
    }
}
