package co.edu.eam.ds.sistemavotacion.services;

import co.edu.eam.ds.sistemavotacion.model.entities.Candidato;
import co.edu.eam.ds.sistemavotacion.model.entities.Partido;
import co.edu.eam.ds.sistemavotacion.repositories.CandidatoRepository;
import co.edu.eam.ds.sistemavotacion.repositories.PartidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CandidatoService {

    @Autowired //injectar la dependencia
    private CandidatoRepository candidatoRepository;
    private Candidato candidato;

    @Autowired
    PartidoRepository partidoRepository;

    @Autowired
    PartidoService partidoService;

    public void create(Candidato candidato) {

        Optional<Candidato> codigos = candidatoRepository.findById(candidato.getCodigo());

        if (!codigos.isEmpty()) {
            throw new RuntimeException("Este codigo ya esta usado...");
        }

        candidatoRepository.save(candidato);
    }

    public void edit(Candidato candidato) {
        Optional<Candidato> candidatoToFind = candidatoRepository.findById(candidato.getCodigo());

        if (candidatoToFind.isEmpty()) {
            throw new EntityNotFoundException("No existe el candidato enviado para votar");
        }

        for (int i = 1; i <= candidatoToFind.hashCode(); i++) {

            candidato.setNombre(candidatoToFind.get().getNombre());
            candidato.setPartido(candidatoToFind.get().getPartido());

            String validacion=candidatoToFind.get().getNumerovotos()+"";

         if (validacion=="null" || validacion.isEmpty()) {
                candidato.setNumerovotos(1);
            }else{
             candidato.setNumerovotos(candidatoToFind.get().getNumerovotos()+1);
         }

        }

        candidatoRepository.save(candidato);

        Partido partido = new Partido();
        partido.setCodigo(candidato.getPartido().getCodigo());
        partidoService.edit(partido);



    }

    public Candidato get(String codigo) {
        return candidatoRepository.findById(codigo).orElse(null);
    }

   /* public List<Candidato> getAll() {
        List<Candidato> candidatos = candidatoRepository.(candidato.getEstado());

        List result = new ArrayList();

        candidatos.forEach(result::add); // for (Customer customer: customers) result.add(customer)

        return result;
    }*/
    public List<Candidato> getActivo() {
        List<Candidato> candidatos = candidatoRepository.findByEstado("activo");

        List result = new ArrayList();

        candidatos.forEach(result::add);

        return result;
    }


}
