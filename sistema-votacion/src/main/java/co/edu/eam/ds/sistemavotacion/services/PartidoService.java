package co.edu.eam.ds.sistemavotacion.services;

import co.edu.eam.ds.sistemavotacion.model.entities.Partido;
import co.edu.eam.ds.sistemavotacion.repositories.PartidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PartidoService {


    @Autowired //injectar la dependencia
    private PartidoRepository partidoRepository;

    public void create(Partido partido) {

        Optional<Partido> nombres = partidoRepository.findById(partido.getCodigo());

        if (!nombres.isEmpty()) {
            throw new RuntimeException("Este partido ya esta usado...");
        }

        partidoRepository.save(partido);
    }

    public void edit(Partido partido) {
        Optional<Partido> partidoToFind = partidoRepository.findById(partido.getCodigo());

        if (partidoToFind.isEmpty()) {
            throw new EntityNotFoundException("No existe el partido");
        }
        for (int i = 1; i <= partidoToFind.hashCode(); i++) {

            partido.setNombre(partidoToFind.get().getNombre());

            String validacion=partidoToFind.get().getTotalvotos()+"";

            if (validacion=="null" || validacion.isEmpty()) {
                partido.setTotalvotos(1);
            }else{
                partido.setTotalvotos(partidoToFind.get().getTotalvotos()+1);
            }

        }

        partidoRepository.save(partido);
    }

    public Partido get(String codigo) {
        return partidoRepository.findById(codigo).orElse(null);
    }

    public List<Partido> getAll() {
        Iterable<Partido> partidos = partidoRepository.findAll();
        List result = new ArrayList();
        partidos.forEach(result::add); // for (Customer customer: customers) result.add(customer)

        return result;
    }
}
