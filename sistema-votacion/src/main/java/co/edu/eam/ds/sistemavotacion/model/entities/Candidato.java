package co.edu.eam.ds.sistemavotacion.model.entities;

import javax.persistence.*;
import java.io.Serializable;
@Table(name= "candidato")
@Entity
public class Candidato implements Serializable {
    @Id
    private String codigo ;
    @Column(name="nombre")
    private String nombre;

    @JoinColumn(name = "partido_codigo")
    @ManyToOne
    private Partido partido;
    @Column(name="estado")
    private String estado;

    @Column(name = "numerovotos")
    private int numerovotos;

    public Candidato() {

    }

    public Candidato(String codigo, String nombre, Partido partido, String estado, int numerovotos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.partido = partido;
        this.estado = estado;
        this.numerovotos = numerovotos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Partido getPartido() {
        return partido;
    }

    public void setPartido(Partido partido) {
        this.partido = partido;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getNumerovotos() {
        return numerovotos;
    }

    public void setNumerovotos(int numerovotos) {
        this.numerovotos = numerovotos;
    }

    //CREATE TABLE public.candidato (
    //	codigo varchar NOT null primary key,
    //	nombre varchar NULL,
    //	partido_codigo varchar NULL
    //);
}
