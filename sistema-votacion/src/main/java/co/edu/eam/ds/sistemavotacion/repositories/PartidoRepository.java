package co.edu.eam.ds.sistemavotacion.repositories;

import co.edu.eam.ds.sistemavotacion.model.entities.Partido;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PartidoRepository extends CrudRepository<Partido , String> {
    Optional<Partido> findById(String codigo);

}
