package co.edu.eam.ds.sistemavotacion.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name= "partido")
@Entity

public class Partido implements Serializable {
    @Id
    private String codigo ;
    @Column(name="nombre")
    private String nombre;
    @Column(name = "totalvotos")
    private int totalvotos;


    public Partido() {

    }

    public Partido(String codigo, String nombre, int totalvotos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.totalvotos = totalvotos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTotalvotos() {
        return totalvotos;
    }

    public void setTotalvotos(int totalvotos) {
        this.totalvotos = totalvotos;
    }
}
