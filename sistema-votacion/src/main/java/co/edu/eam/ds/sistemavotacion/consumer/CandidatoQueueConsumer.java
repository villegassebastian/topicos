package co.edu.eam.ds.sistemavotacion.consumer;

import co.edu.eam.ds.sistemavotacion.model.entities.Candidato;
import co.edu.eam.ds.sistemavotacion.model.msj.CandidatoMessage;
import co.edu.eam.ds.sistemavotacion.services.CandidatoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class CandidatoQueueConsumer {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CandidatoService candidatoService;

    @KafkaListener(topics = "2aml5a5q-votos")
    public void ListenCandidatosQueue(String message) throws JsonProcessingException {
        System.out.println(message);

        CandidatoMessage c= objectMapper.readValue(message, CandidatoMessage.class);

        candidatoService.edit(new Candidato(c.getCodigo(),c.getNombre(),c.getPartidos(),c.getEstado(),c.getNumerovotos()));
    }
}
