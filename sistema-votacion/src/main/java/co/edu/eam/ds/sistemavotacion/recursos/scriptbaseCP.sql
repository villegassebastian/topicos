create database sistemavotacion2;

-- public.candidato definition

-- Drop table

-- DROP TABLE public.candidato;

CREATE TABLE public.candidato (
	codigo varchar NOT NULL,
	nombre varchar NULL,
	partido_codigo varchar NULL,
	estado varchar NOT NULL,
	numerovotos int4 NULL DEFAULT 0,
	CONSTRAINT candidato_pkey PRIMARY KEY (codigo)
);


-- public.partido definition

-- Drop table

-- DROP TABLE public.partido;

CREATE TABLE public.partido (
	codigo varchar NOT NULL,
	nombre varchar NOT NULL,
	totalvotos int4 NULL DEFAULT 0,
	CONSTRAINT partido_pkey PRIMARY KEY (codigo)
);

INSERT INTO public.partido (codigo,nombre,totalvotos) VALUES
	 ('1','partido U',0),
	 ('2','liberal',0),
	 ('3','democratico',0);

INSERT INTO public.candidato (codigo,nombre,partido_codigo,estado,numerovotos) VALUES
	 ('3','petro','2','activo',0),
	 ('4','juan manuel','1','activo',0),
	 ('1','ivan duque','3','activo',0),
	 ('2','alvaro uribe','3','activo',0);