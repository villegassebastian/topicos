package co.edu.eam.ds.sistemavotacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaVotacionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemaVotacionApplication.class, args);
	}

}
