package co.edu.eam.ds.sistemavotacion.configs;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Bean
    public Queue candidatoQueue(){

        return new Queue("voting_info_queue_villegas", true);
    }
}
