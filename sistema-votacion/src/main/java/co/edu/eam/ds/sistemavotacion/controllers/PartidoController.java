package co.edu.eam.ds.sistemavotacion.controllers;

import co.edu.eam.ds.sistemavotacion.model.entities.Partido;
import co.edu.eam.ds.sistemavotacion.services.PartidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/partido")
public class PartidoController {

    @Autowired
    private PartidoService partidoService;


    @PostMapping
    public void create(@RequestBody Partido partido) {
        partidoService.create(partido);
    }

    @PutMapping("/{codigo}")
    public void edit(@PathVariable String codigo, @RequestBody Partido partido) {
        partido.setCodigo(codigo);
        partidoService.edit(partido);
    }

    @GetMapping //GET -> /customer
    public Partido get(@RequestParam("codigo") String codigo) {
        return partidoService.get(codigo);
    }

    @GetMapping("/all")  // GET -> /customer
    public List<Partido> getAll() {

        return partidoService.getAll();
    }


}
