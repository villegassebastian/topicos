package co.edu.eam.ds.sistemavotacion.controllers;

import co.edu.eam.ds.sistemavotacion.model.entities.Candidato;
import co.edu.eam.ds.sistemavotacion.services.CandidatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/candidato")

public class CandidatoController {


        @Autowired
        private CandidatoService candidatoService;


        @PostMapping
        public void create(@RequestBody Candidato candidato) {
            candidatoService.create(candidato);
        }

        @PutMapping("/{codigo}")
        public void edit(@PathVariable String codigo, @RequestBody Candidato candidato) {
            candidato.setCodigo(codigo);
            candidatoService.edit(candidato);
        }

        @GetMapping //GET -> /customer
        public Candidato get(@RequestParam("codigo") String codigo) {
            return candidatoService.get(codigo);
        }

       /* @GetMapping("/all")  // GET -> /customer
        public List<Candidato> getAll() {

            return candidatoService.getAll();
        }*/
        @GetMapping("/all")
       public List<Candidato> getActivo(){
            return candidatoService.getActivo();


        }
    }


