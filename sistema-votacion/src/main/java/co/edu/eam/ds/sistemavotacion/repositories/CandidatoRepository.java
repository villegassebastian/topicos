package co.edu.eam.ds.sistemavotacion.repositories;

import co.edu.eam.ds.sistemavotacion.model.entities.Candidato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidatoRepository  extends CrudRepository<Candidato, String> {

    //@Query("SELECT c FROM candidato c WHERE c.estado like :estado")
    List<Candidato>findByEstado(String estado);



}
