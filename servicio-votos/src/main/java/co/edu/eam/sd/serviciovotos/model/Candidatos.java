package co.edu.eam.sd.serviciovotos.model;

public class Candidatos {

    private String codigo;
    private String nombre;
    private Partidos partidos;
    private String estado;
    private int numerovotos;

    public Candidatos() {
    }

    public Candidatos(String codigo, String nombre, Partidos partidos, String estado, int numerovotos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.partidos = partidos;
        this.estado = estado;
        this.numerovotos = numerovotos;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Partidos getPartidos() {
        return partidos;
    }

    public void setPartidos(Partidos partidos) {
        this.partidos = partidos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getNumerovotos() {
        return numerovotos;
    }

    public void setNumerovotos(int numerovotos) {
        this.numerovotos = numerovotos;
    }
}
