package co.edu.eam.sd.serviciovotos.model;

public class Partidos {

    private String codigo;
    private String nombre;


    public Partidos() {
    }

    public Partidos(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
