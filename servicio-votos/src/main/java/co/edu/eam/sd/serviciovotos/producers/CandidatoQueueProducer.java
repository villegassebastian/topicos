package co.edu.eam.sd.serviciovotos.producers;

import co.edu.eam.sd.serviciovotos.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class CandidatoQueueProducer {

    /**@Autowired
    private RabbitTemplate rabbitTemplate;
     **/

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    /**public void enviarCandidato(Votos votos) throws JsonProcessingException {

        Candidatos candidatos=new Candidatos();
        Partidos partidos=new Partidos();

        partidos.setCodigo("");

        candidatos.setCodigo(votos.getId_candidato());
        candidatos.setNombre("");
        candidatos.setPartidos(partidos);
        candidatos.setEstado("activo");
        candidatos.setNumerovotos(0);

        String message=objectMapper.writeValueAsString(candidatos);
        System.out.println(message);

        rabbitTemplate.convertAndSend("voting_info_queue_villegas", message);
    }
**/

    public void enviarCandidato(Votos votos) throws JsonProcessingException {
        Candidatos candidatos=new Candidatos();
        Partidos partidos=new Partidos();

        partidos.setCodigo("");

        candidatos.setCodigo(votos.getId_candidato());
        candidatos.setNombre("");
        candidatos.setPartidos(partidos);
        candidatos.setEstado("activo");
        candidatos.setNumerovotos(0);

        kafkaTemplate.send("2aml5a5q-votos", objectMapper.writeValueAsString(votos));
    }
}
