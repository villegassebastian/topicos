package co.edu.eam.sd.serviciovotos.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name="votos")
@Entity
public class Votos implements Serializable {

    @Id
    private String cedula_votante;

    @Column(name = "id_candidato")
    private String id_candidato;

    @Column(name = "id_puesto_votacion")
    private String id_puesto_votacion;

    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;


    public Votos() {
    }

    public Votos(String cedula_votante, String id_candidato, String id_puesto_votacion, Date fecha) {
        this.cedula_votante = cedula_votante;
        this.id_candidato = id_candidato;
        this.id_puesto_votacion = id_puesto_votacion;
        this.fecha = fecha;
    }

    public String getCedula_votante() {
        return cedula_votante;
    }

    public void setCedula_votante(String cedula_votante) {
        this.cedula_votante = cedula_votante;
    }

    public String getId_candidato() {
        return id_candidato;
    }

    public void setId_candidato(String id_candidato) {
        this.id_candidato = id_candidato;
    }

    public String getId_puesto_votacion() {
        return id_puesto_votacion;
    }

    public void setId_puesto_votacion(String id_puesto_votacion) {
        this.id_puesto_votacion = id_puesto_votacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
