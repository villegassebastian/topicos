package co.edu.eam.sd.serviciovotos.model;

public class Votante {

    private String id;
    private String nombre;
    private Puestovotacion puestovotacion;
    private boolean cambio;
    private boolean voto;

    public Votante() {
    }

    public Votante(String id, String nombre, Puestovotacion puestovotacion, boolean cambio, boolean voto) {
        this.id = id;
        this.nombre = nombre;
        this.puestovotacion = puestovotacion;
        this.cambio = cambio;
        this.voto = voto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Puestovotacion getPuestovotacion() {
        return puestovotacion;
    }

    public void setPuestovotacion(Puestovotacion puestovotacion) {
        this.puestovotacion = puestovotacion;
    }

    public boolean isCambio() {
        return cambio;
    }

    public void setCambio(boolean cambio) {
        this.cambio = cambio;
    }

    public boolean isVoto() {
        return voto;
    }

    public void setVoto(boolean voto) {
        this.voto = voto;
    }
}
