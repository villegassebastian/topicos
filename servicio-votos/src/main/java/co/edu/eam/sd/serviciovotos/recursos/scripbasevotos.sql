create database votos

-- public.votos definition

-- Drop table

-- DROP TABLE public.votos;

CREATE TABLE public.votos (
	cedula_votante varchar NOT NULL,
	id_candidato varchar NOT NULL,
	id_puesto_votacion varchar NOT NULL,
	fecha timestamp(0) NOT NULL,
	CONSTRAINT votos_pkey PRIMARY KEY (cedula_votante)
);

