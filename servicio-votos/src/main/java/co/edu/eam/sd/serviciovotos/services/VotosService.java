package co.edu.eam.sd.serviciovotos.services;

import co.edu.eam.sd.serviciovotos.model.Votos;
import co.edu.eam.sd.serviciovotos.repositories.VotosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class VotosService {

    @Autowired
    private VotosRepository votosRepository;

    public void guardarVoto(Votos votos){

        votosRepository.save(votos);
    }
}
