package co.edu.eam.sd.serviciovotos.services;

import co.edu.eam.sd.serviciovotos.model.Votos;
import co.edu.eam.sd.serviciovotos.producers.CandidatoQueueProducer;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandidatoService {

    @Autowired
    private CandidatoQueueProducer candidatoQueueProducer;


    public void modificarCandidato(Votos votos) throws JsonProcessingException {
        candidatoQueueProducer.enviarCandidato(votos);
    }
}
