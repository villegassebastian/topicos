package co.edu.eam.sd.serviciovotos.controllers;

import co.edu.eam.sd.serviciovotos.model.Votante;
import co.edu.eam.sd.serviciovotos.model.Votos;
import co.edu.eam.sd.serviciovotos.services.CandidatoService;
import co.edu.eam.sd.serviciovotos.services.VotanteService;
import co.edu.eam.sd.serviciovotos.services.VotosService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
@RequestMapping("/votos")
public class VotosController {

    @Autowired
    private VotosService votosService;

    @Autowired
    private VotanteService votanteService;

    @Autowired
    private CandidatoService candidatoService;

    @PostMapping
    public void guardarVotos(@RequestBody Votos votos) throws JsonProcessingException {
        Date fecha=new Date();
        votos.setFecha(fecha);
        votanteService.modificarVotante(votos);
        candidatoService.modificarCandidato(votos);
        votosService.guardarVoto(votos);


    }
}
