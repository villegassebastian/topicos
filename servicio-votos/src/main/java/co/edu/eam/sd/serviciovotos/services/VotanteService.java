package co.edu.eam.sd.serviciovotos.services;

import co.edu.eam.sd.serviciovotos.model.Votante;
import co.edu.eam.sd.serviciovotos.model.Votos;
import co.edu.eam.sd.serviciovotos.producers.VotanteQueueProducer;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VotanteService {

    @Autowired
    private VotanteQueueProducer votanteQueueProducer;

    //public void crearVotante(Votante votante) throws JsonProcessingException {
      //  votanteQueueProducer.enviarVotante(votante);
    //}

    public void modificarVotante(Votos votos) throws JsonProcessingException {
        votanteQueueProducer.enviarVotante(votos);

    }
}
