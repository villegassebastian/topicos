package co.edu.eam.sd.serviciovotos.repositories;

import co.edu.eam.sd.serviciovotos.model.Votos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotosRepository extends CrudRepository<Votos, String> {
}
