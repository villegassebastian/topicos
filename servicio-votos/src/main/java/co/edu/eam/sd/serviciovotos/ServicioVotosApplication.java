package co.edu.eam.sd.serviciovotos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicioVotosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicioVotosApplication.class, args);
	}

}
