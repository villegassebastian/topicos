package co.edu.eam.sd.serviciovotos.producers;

import co.edu.eam.sd.serviciovotos.model.Puestovotacion;
import co.edu.eam.sd.serviciovotos.model.Votante;
import co.edu.eam.sd.serviciovotos.model.Votos;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class VotanteQueueProducer {

   /** @Autowired
    private RabbitTemplate rabbitTemplate;
   **/

   @Autowired
   private KafkaTemplate<String,String> kafkaTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    /**public void enviarVotante(Votos votos) throws JsonProcessingException {

        Votante votante = new Votante();
        Puestovotacion puestovotacion = new Puestovotacion();

        puestovotacion.setId(votos.getId_puesto_votacion());
        puestovotacion.setNombrepuesto("");
        puestovotacion.setLugar("");
        puestovotacion.setMunicipio("");
        puestovotacion.setDepartamento("");

        votante.setId(votos.getCedula_votante());
        votante.setNombre("");
        votante.setPuestovotacion(puestovotacion);
        votante.setCambio(false);
        votante.setVoto(true);

        String message=objectMapper.writeValueAsString(votante);

        rabbitTemplate.convertAndSend("votante_queue_villegas", message);
    }**/
    public void enviarVotante(Votos votos) throws JsonProcessingException {
        Votante votante = new Votante();
        Puestovotacion puestovotacion = new Puestovotacion();

        puestovotacion.setId(votos.getId_puesto_votacion());
        puestovotacion.setNombrepuesto("");
        puestovotacion.setLugar("");
        puestovotacion.setMunicipio("");
        puestovotacion.setDepartamento("");

        votante.setId(votos.getCedula_votante());
        votante.setNombre("");
        votante.setPuestovotacion(puestovotacion);
        votante.setCambio(false);
        votante.setVoto(true);
        kafkaTemplate.send("2aml5a5q-votos", objectMapper.writeValueAsString(votos));
    }
}
